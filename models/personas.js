const { guardarInfo } = require('./guardarInfo');
const Persona = require('./persona')

class Personas {

  constructor() {
    this._listado = [];
  } 

  crearPersona(persona = {}) {
    // console.log(persona);

    this._listado.push(persona)
    // console.log(this._listado, '9999');SI
    // this._listado[person.id] = person
    // console.log('persona', person, 'persona'); //not defined
  }

  get personasArr() {
    const listado = [];

    Object.keys(this._listado).forEach(key => {
    //  console.log('llave', key, 'llave');SI
      const person = this._listado[key];
      listado.push(person);
    });
    // console.log('alba');
    // console.log('array', listado, 'array');SI
    return listado;
}

cargarTareasFormArray(personas = []){

  personas.forEach(persona => {
    this._listado[persona.id] = persona;
  }) 
} 

editarPersona(persona, index) {
  this._listado[index].nombres = persona.nombres,
  this._listado[index].apellidos = persona.apellidos,
  this._listado[index].ci = persona.ci,
  this._listado[index].direccion = persona.direccion,
  this._listado[index].sexo = persona.sexo
  guardarInfo(this._listado)
}

eliminarPersona(id){
  // console.log('el indice', id);
  // if (this._listado[id]) {
  //   console.log('el listad', this._listado[id], 'el listado');
  //   delete this._listado[id];
  //   console.log('el listad', this._listado[id], 'el listado');
  // }
  
  this._listado = this._listado.filter(data =>  {
    return data.id!== id
  })


}

}

module.exports = Personas;