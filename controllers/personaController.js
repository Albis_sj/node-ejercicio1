const { request } = ("express");
const req = require("express/lib/request");
const { leerInfo, guardarInfo } = require("../models/guardarInfo");
const Persona = require("../models/persona");
const Personas = require("../models/personas");

const { response } = 'express';
//en el db mandar una copia de la lista
const personas = new Personas(); //lista


const tareasDB = leerInfo()
if(tareasDB){
  //establecer las tareas
  personas.cargarTareasFormArray(tareasDB);
  // console.log('DDDB', tareasDB, 'DBBBd');
  personas._listado = tareasDB
  // console.log('22', personas._listado, '2222');
}

const personaGet = (req, res = response) => {

  const lista = leerInfo();
  // console.log(lista, 'hahah');
  res.json({
    msg: 'post API - Controldor',
    lista,
  });
};

const personaPut =  (req = request, res = response) => {
  const  {id} = req.params;
  const { nombres, apellidos, ci, direccion, sexo } = req.body;
  const index = personas._listado.findIndex(object => object.id === id)

  if (index !== -1){
    personas.editarPersona({ nombres, apellidos, ci, direccion, sexo }, index)
    res.json({
      msg: 'persona editada',
    })
  } else {
    res.json ({
      msg: 'person no encontrada',
    })
  }

};

const personaPost = (req, res = response) => {

  const { nombres, apellidos, ci, direccion, sexo } = req.body;

  const persona = new Persona( nombres, apellidos, ci, direccion, sexo )
  // console.log('3333', persona, '333');

//crear el metodo crearpersona
  personas.crearPersona(persona)
  // console.log('000',personas._listado, '000');
  guardarInfo(personas._listado)
  const lista = personas._listado
  
  res.json({
    msg: 'persona Creada',
    // lista,
  });

  
};

const personaDelete = (req, res = response) => {
  console.log('eliminar');



  const { id } = req.params
  // console.log('idd', id, 'alba');
  if (id) {
    personas.eliminarPersona(id);
    guardarInfo(personas.personasArr)

    res.json({
    msg: 'persona eliminada - Controlador',
    })
  }

};




module.exports = {
  personaGet,
  personaPut,
  personaPost,
  personaDelete,
}




