const { Router } = require('express');
const { personaGet, personaPut, personaPost, personaDelete, personaGetNA, personaGetCi, personaGetSexo,  personaGetObtGen } = require('../controllers/personaController');
// console.log('personaRoutes, 'alba');

const router = Router();

router.get('/', personaGet);

router.put('/:id', personaPut);

router.post('/', personaPost);

router.delete('/:id', personaDelete);



module.exports = router;